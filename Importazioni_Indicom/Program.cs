﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;

namespace Importazioni_Incom
{
    class Program
    {

        XmlDocument docXml = new XmlDocument();
        SAPbobsCOM.Company oCompany;
        private SAPbobsCOM.Documents oDoc;
        private SAPbobsCOM.Documents oPN;
        private SAPbobsCOM.Recordset oRecordset;


        string fileLogDB;

        static void Main(string[] args)
        {
            var prog = new Program();

            prog.docXml.Load("parametriDB.xml");
            

            Log("  Avvio Importazione");

            prog.ConnessioneSAP(prog.docXml);
            prog.FattVendita();
            prog.PrimaNota();
            prog.DisconessioneSAP();

        }

        static void Log(string logMessage)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;

            string logFile = "c:/temp/Log_Importazioni" + "_" + dt.ToString("yyyyMMdd") + ".txt";

            using (StreamWriter w = File.AppendText(logFile))
            {

                w.WriteLine(logMessage);

            }
            Console.WriteLine(logMessage);
        }

        public void ConnessioneSQL(XmlDocument docXml)
        {
            XmlNodeList elemlist = docXml.GetElementsByTagName("SQL");
            foreach (XmlNode node in elemlist)
            {

            }
        }

        public void ConnessioneSAP(XmlDocument docXml)
        {
            oCompany = new SAPbobsCOM.Company();
            XmlNodeList elemlist = docXml.GetElementsByTagName("SAP");
            foreach (XmlNode node in elemlist)
            {
                oCompany.Server = node.Attributes["Server"].Value;
                oCompany.DbUserName = node.Attributes["DbUsername"].Value;
                oCompany.DbPassword = node.Attributes["DbPassword"].Value;
                oCompany.CompanyDB = node.Attributes["CompanyDB"].Value;
                oCompany.UserName = node.Attributes["Username"].Value;
                oCompany.Password = node.Attributes["Password"].Value;
            }
            oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
            oCompany.UseTrusted = false;

            int errCode = oCompany.Connect();
            if (errCode != 0)
            {
                string err;
                int code;
                oCompany.GetLastError(out code, out err);
                Log("   Connessione database non riuscita,  errore: " + code + " - " + err);
            }
            else
            {
                Log("   Connessione al database SAP: OK ");
            }
        }

        private void FattVendita() //Inserimento Testata fatture vendita
        {

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //dichiarazioni variabili 
            DateTime docDate;
            DateTime taxDate;
            DateTime vatDate;

            //Conversioni variabili
            docDate = Convert.ToDateTime("dataSQL");
            taxDate = Convert.ToDateTime("dataivaSQL");
            vatDate = Convert.ToDateTime("vatdateSQL");

            //Inizio Inserimento documento
            oCompany.StartTransaction();
            SAPbobsCOM.BoObjectTypes tipoDocumento;
            tipoDocumento = SAPbobsCOM.BoObjectTypes.oInvoices;
            oDoc = oCompany.GetBusinessObject(tipoDocumento);
            oDoc.CardCode = "";
            oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
            oDoc.DocDate = docDate;
            oDoc.TaxDate = taxDate;
            oDoc.CardCode = "CardCode";
            oDoc.NumAtCard = "rifFatturaSQL";
            oDoc.DocCurrency = "ValutaSQL";
            oDoc.DocRate = 0; //Cambio SQL
            oDoc.PaymentGroupCode = 0000; //Codice condizione pagamento
            oDoc.Series = 000; //Serie 
            oDoc.PaymentMethod = "METODOPAGAMENTOSQL";
            oDoc.VatDate = vatDate;

            RigheFattVendita(oDoc); // void inserimento fatture

            int res = oDoc.Add();
            if (res == 0)
            {
                oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit); //Convalida l'operazione 
                oRecordset.DoQuery("");

                Log("   " + "Fattura Vendita" + "    n: " + "DocNumSQL" + "   Inserita correttamente");
            }
            else
            {
                Log("ERRORE in inserimento Fattura di Vendita     n: "+ "DocNumSQL  " + oCompany.GetLastErrorCode() + " - " + oCompany.GetLastErrorDescription());
            }
        }

        public void RigheFattVendita(SAPbobsCOM.Documents oDoc)
        {

            if ("DocNumT"=="DocNumT")
            {

                oDoc.Lines.ItemDescription = "Descrizione SQL";
                oDoc.Lines.AccountCode = "CodiceConto";
                oDoc.Lines.VatGroup = "CodIVA";
                oDoc.Lines.LineTotal = 000;
                oDoc.Lines.GrossTotalFC = 000;
                oDoc.Lines.ProjectCode = "123";
                oDoc.Lines.Add();

            }

        }

        public void PrimaNota()
        {
            //dichiarazioni variabili 
            DateTime refDate;
            DateTime taxDate;
            DateTime vatDate;
            DateTime dueDate;

            //Conversioni variabili
            refDate = Convert.ToDateTime("refDateSQL");
            taxDate = Convert.ToDateTime("dataivaSQL");
            vatDate = Convert.ToDateTime("vatdateSQL");
            dueDate = Convert.ToDateTime("duedateSQL");


            oPN = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries);
            oPN.OriginalRefDate = refDate;
            oPN.TaxDate = taxDate;
            oPN.VatDate = vatDate;
            oPN.DocDueDate = dueDate;
            oPN.Reference1 = "numFatt";
            oPN.Project = "Progetto";
            oPN.JournalMemo = "osservazioni";

        }

        public void RighePrimaNota()
        {
            //dichiarazioni sap
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //Dichiarazioni Variabili
            string CodConto;
            double Dare = 0;
            double Avere = 0;

            //Recordset SAP
            oRecordset.DoQuery("SELECT T0.\"AcctCode\" FROM OACT T0 WHERE T0.\"AccntntCod\" = '" + "CodiceSQL" + "' ");
            CodConto = oRecordset.Fields.Item(0).Value;


            oPN.Lines.AccountCode = CodConto;

            if (Dare > 0)
            {
                
            }
        }

        public void ceckDoc()
        {
            //Controllo Esistenda documento in SAP
            string DocNum;
            

        }

        public void DisconessioneSAP()
        {
            oCompany.Disconnect();
            oCompany = null;
            Log("   Disconnessione dal database SAP: OK");
        }


    }
}
